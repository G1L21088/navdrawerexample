package mx.gilsantaella.navdrawerexample;

import android.app.Activity;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

public class MainActivity extends Activity {

    RelativeLayout rightRL;
    DrawerLayout drawerLayout;
    ImageView icRight;
    ListView ltvNavMenu;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//      I'm removing the ActionBar.
        setContentView(R.layout.activity_main);

        rightRL = (RelativeLayout)findViewById(R.id.whatYouWantInRightDrawer);
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        icRight = (ImageView) findViewById(R.id.ic_menu_right);
        ltvNavMenu = (ListView) findViewById(R.id.ltvNavMenu);
        adapter = new ArrayAdapter(this,R.layout.nav_menu_list, getResources().getStringArray(R.array.elementos));
        ltvNavMenu.setAdapter(adapter);

        ltvNavMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("Resultado", "" + position);
                switch (position) {
                    case 0:
                        Log.e("Resultado", "FichaTecnica" + position);
                        break;
                    case 1:
                        Log.e("Resultado", "Fotos" + position);
                        break;
                    case 2:
                        Log.e("Resultado", "Mapa" + position);
                        break;
                    case 3:
                        Log.e("Resultado", "A,B,C" + position);
                        break;
                    case 4:
                        Log.e("Resultado", "Observaciones" + position);
                        break;
                    case 5:
                        Log.e("Resultado", "Guardar" + position);
                        break;
                    case 6:
                        Log.e("Resultado", "Sincronizar" + position);
                        break;
                    case 7:
                        Log.e("Resultado", "Ayuda" + position);
                        break;
                    default:
                        break;
                }
            }
        });

        icRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(rightRL))
                    drawerLayout.closeDrawer(rightRL);
                else
                    drawerLayout.openDrawer(rightRL);
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Checking for the "menu" key
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            if (drawerLayout.isDrawerOpen(rightRL))
                drawerLayout.closeDrawer(rightRL);
            else
                drawerLayout.openDrawer(rightRL);
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(rightRL))
            drawerLayout.closeDrawer(rightRL);
        else
            super.onBackPressed();
    }
}